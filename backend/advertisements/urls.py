from advertisements.views import OfferViewSet, CategoryViewSet
from django.urls import path

urlpatterns = [
    path('offers/', OfferViewSet.as_view({'get': 'list', 'post': 'create'}), name='offer_list'),
    path('offers/<int:pk>/', OfferViewSet.as_view({'get': 'retrieve', 'put': 'update', 'delete': 'destroy'}),
         name='offer_detail'),
    path('category/', CategoryViewSet.as_view({'get': 'list', 'post': 'create'}), name='category_list'),
    path('category/<int:pk>/', CategoryViewSet.as_view({'put': 'update', 'delete': 'destroy'}), name='category_detail'),
]
