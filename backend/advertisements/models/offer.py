from datetime import date

from advertisements.models import Category
from django.db import models


class Offer(models.Model):
    """
        Stores information about offers.
    """
    title = models.CharField(max_length=100)
    description = models.CharField(max_length=400)
    price = models.FloatField()
    created_at = models.DateField(default=date.today)
    category = models.ForeignKey(Category, on_delete=models.CASCADE)

    def __str__(self):
        return self.title
