from django.db import models


class Category(models.Model):
    """
        Stores information about categories.
    """
    name = models.CharField(max_length=100)
    ordering = models.IntegerField(unique=True, blank=True, null=True)

    def __str__(self):
        return f"{self.name} - {self.ordering}"

    class Meta:
        ordering = ['ordering']
