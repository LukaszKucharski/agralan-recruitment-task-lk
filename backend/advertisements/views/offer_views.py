from advertisements.models import Offer, Category
from advertisements.serializers import OfferSerializer, OfferDetailSerializer
from django.http import Http404
from django.shortcuts import get_object_or_404
from rest_framework import status
from rest_framework import viewsets
from rest_framework.response import Response


class OfferViewSet(viewsets.ViewSet):
    """
    A simple ViewSet for listing or retrieving offers.
    """

    def list(self, request):
        queryset = Offer.objects.all()
        if "category" in request.query_params.keys():
            category = get_object_or_404(Category, name=str(request.query_params['category']))
            queryset = queryset.filter(category=category)

        serializer = OfferSerializer(queryset, many=True)
        return Response(serializer.data)

    def retrieve(self, request, pk=None):
        queryset = Offer.objects.all()
        offer = get_object_or_404(queryset, pk=pk)
        serializer = OfferDetailSerializer(offer)
        return Response(serializer.data)

    def create(self, request):
        serializer = OfferDetailSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def update(self, request, pk):
        offer = self.get_object(pk)
        serializer = OfferDetailSerializer(offer, data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def destroy(self, request, pk):
        offer = self.get_object(pk)
        offer.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_object(self, pk):
        try:
            return Offer.objects.get(pk=pk)
        except Offer.DoesNotExist:
            raise Http404
