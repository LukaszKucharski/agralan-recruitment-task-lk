from advertisements.models import Offer
from rest_framework import serializers


class OfferSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = ("id", "title", "price", "category")


class OfferDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Offer
        fields = ("__all__")
