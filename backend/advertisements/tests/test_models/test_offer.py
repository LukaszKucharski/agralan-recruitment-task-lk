from advertisements.models import Offer, Category
from advertisements.tests.factories import OfferFactory
from django.test import TestCase


class OfferModelTest(TestCase):
    """
        Offer model test
    """

    def test_model_creation(self):
        self.assertEqual(Offer.objects.count(), 0)

        offer: Offer = OfferFactory()
        self.assertTrue(isinstance(offer, Offer))
        self.assertEqual(Offer.objects.count(), 1)
        self.assertEqual(offer.title, 'Arduino')
        self.assertEqual(offer.description, "Arduino is an open-source...")
        self.assertEqual(offer.price, 98.99)
        self.assertEqual(offer.__str__(), "Arduino")
        self.assertTrue(isinstance(offer.category, Category))
        self.assertEqual(offer.category.name, 'electronics')


