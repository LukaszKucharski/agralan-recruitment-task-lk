from advertisements.models import Offer, Category
from advertisements.tests.factories import CategoryFactory
from django.test import TestCase
from django.urls import reverse
from rest_framework import status


class CategoryModelTest(TestCase):
    """
        Category model test
    """

    def test_model_creation(self):
        self.assertEqual(Category.objects.count(), 0)

        category: Category = CategoryFactory()
        self.assertTrue(isinstance(category, Category))
        self.assertEqual(Category.objects.count(), 1)
        self.assertEqual(category.name, 'electronics')
        self.assertEqual(category.__str__(), "electronics - None")

        second_category: Category = CategoryFactory(name='mechanics', ordering=2)
        self.assertTrue(isinstance(second_category, Category))
        self.assertEqual(Category.objects.count(), 2)
        self.assertEqual(second_category.name, 'mechanics')
        self.assertEqual(second_category.ordering, 2)
        self.assertEqual(second_category.__str__(), "mechanics - 2")

    def test_create_category(self):
        url = reverse("category_list")
        data = {"name": "mechanics"}
        category_number = Category.objects.count()
        response = self.client.post(url, data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(category_number + 1, Category.objects.count())
        category = Category.objects.get(id=response.data["id"])
        self.assertEqual(category.name, data["name"])

    def test_update_offer(self):
        category: Category = CategoryFactory()
        url = f"/api/category/{category.id}/"
        data = {"name": "tools"}
        response = self.client.put(url, data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        category = Category.objects.get(id=response.data["id"])
        self.assertEqual(category.name, data["name"])

    def test_delete_offer(self):
        category: Category = CategoryFactory()
        category_number = Category.objects.count()
        url = f"/api/category/{category.id}/"
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(category_number - 1, Category.objects.count())
