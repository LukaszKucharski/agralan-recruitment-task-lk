from advertisements.models import Offer, Category
from advertisements.tests.factories import OfferFactory, CategoryFactory
from django.test import TestCase
from django.urls import reverse
from rest_framework import status


class OfferApiTests(TestCase):

    def setUp(self):
        self.first_offer: Offer = OfferFactory()
        self.second_offer: Offer = OfferFactory(title="Raspberry Pi")
        category: Category = CategoryFactory(name="3D printers")
        self.third_offer: Offer = OfferFactory(title="Ender v3", category=category)

    def test_get_list_offer(self):
        url = reverse("offer_list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

        first_offer = response.data[0]
        self.assertEqual(first_offer["title"], "Arduino")
        self.assertFalse("description" in first_offer.keys())

        second_offer = response.data[2]
        self.assertEqual(second_offer["title"], "Ender v3")
        self.assertFalse("description" in second_offer.keys())

    def test_get_list_offer_with_parameter(self):
        url = reverse("offer_list")
        response = self.client.get(url, {"category": "3D printers"})
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)

        offer = response.data[0]
        self.assertFalse("description" in offer.keys())
        self.assertEqual(offer["title"], "Ender v3")

    def test_get_detail_offer(self):
        offer = Offer.objects.first().id
        response = self.client.get(f'/api/offers/{offer}/')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        offer = response.data
        self.assertEqual(offer["title"], "Arduino")
        self.assertTrue("description" in offer.keys())
        self.assertTrue("created_at" in offer.keys())
        self.assertEqual(offer["description"], "Arduino is an open-source...")
        self.assertEqual(offer["price"], 98.99)

    def test_create_offer(self):
        category = Category.objects.first().id
        offer_number = Offer.objects.count()
        url = reverse("offer_list")
        data = {"title": "Raspberry Pi Pico", "description": "New module...",
                "price": 12.99, "category": category}
        response = self.client.post(url, data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(offer_number + 1, Offer.objects.count())
        offer = Offer.objects.get(id=response.data["id"])
        self.assertEqual(offer.title, data["title"])
        self.assertEqual(offer.description, data["description"])
        self.assertEqual(offer.price, data["price"])

    def test_update_offer(self):
        category = Category.objects.last().id
        offer = Offer.objects.first().id
        url = f"/api/offers/{offer}/"
        data = {"title": "Raspberry Pi v3", "description": "Old module...", "price": 12.01, "category": category}
        response = self.client.put(url, data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        offer = Offer.objects.get(id=response.data["id"])
        self.assertEqual(offer.title, data["title"])
        self.assertEqual(offer.description, data["description"])
        self.assertEqual(offer.price, data["price"])

    def test_delete_offer(self):
        offer_number = Offer.objects.count()
        offer = Offer.objects.last().id
        url = f"/api/offers/{offer}/"
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(offer_number - 1, Offer.objects.count())
