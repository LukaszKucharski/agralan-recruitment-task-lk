from advertisements.models import Offer, Category
from advertisements.tests.factories import CategoryFactory
from django.test import TestCase
from django.urls import reverse
from rest_framework import status


class CategoryApiTests(TestCase):

    def setUp(self):
        self.first_category: Category = CategoryFactory()
        self.second_category: Offer = CategoryFactory(name="3D printers", ordering=2)
        self.third_category: Offer = CategoryFactory(name="mechanics", ordering=1)

    def test_get_list_offer(self):
        url = reverse("category_list")
        response = self.client.get(url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 3)

        first_category = response.data[0]
        self.assertEqual(first_category["name"], "mechanics")

        second_category = response.data[1]
        self.assertEqual(second_category["name"], "3D printers")

        second_category = response.data[2]
        self.assertEqual(second_category["name"], "electronics")

    def test_create_category(self):
        url = reverse("category_list")
        data = {"name": "mechanics"}
        category_number = Category.objects.count()
        response = self.client.post(url, data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(category_number + 1, Category.objects.count())
        category = Category.objects.get(id=response.data["id"])
        self.assertEqual(category.name, data["name"])

    def test_update_offer(self):
        category: Category = CategoryFactory()
        url = f"/api/category/{category.id}/"
        data = {"name": "tools"}
        response = self.client.put(url, data, content_type='application/json')
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        category = Category.objects.get(id=response.data["id"])
        self.assertEqual(category.name, data["name"])

    def test_delete_offer(self):
        category: Category = CategoryFactory()
        category_number = Category.objects.count()
        url = f"/api/category/{category.id}/"
        response = self.client.delete(url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(category_number - 1, Category.objects.count())