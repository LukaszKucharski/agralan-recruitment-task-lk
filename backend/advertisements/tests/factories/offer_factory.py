import factory
from advertisements.models import Offer
from factory.django import DjangoModelFactory

from .category_factory import CategoryFactory


class OfferFactory(DjangoModelFactory):
    class Meta:
        model = Offer

    title = "Arduino"
    description = "Arduino is an open-source..."
    price = 98.99
    category = factory.SubFactory(CategoryFactory)
