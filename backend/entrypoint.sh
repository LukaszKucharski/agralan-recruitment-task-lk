#!/bin/bash
sleep 5
echo ''
echo '--------------------------'
echo 'Database migration'
echo '--------------------------'
echo ''
python manage.py makemigrations
python manage.py migrate
echo ''
echo '--------------------------'
echo 'Localization migration'
echo '--------------------------'
echo ''
django-admin makemessages -l pl
django-admin compilemessages
echo ''
echo '--------------------------'
echo 'Generate static files folder'
echo '--------------------------'
echo ''
./manage.py collectstatic --noinput
echo ''
echo '--------------------------'
echo 'Create admin account'
echo '--------------------------'
echo ''
echo "from django.contrib.auth import get_user_model; User = get_user_model(); User.objects.create_superuser('admin', 'admin@myproject.com', 'admin')" | python manage.py shell
echo ''
echo '--------------------------'
echo 'Run server'
echo '--------------------------'
echo ''
python manage.py runserver 0.0.0.0:80
