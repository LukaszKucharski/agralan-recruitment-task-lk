import { Component, Input, OnInit } from '@angular/core';
import {RestService, OfferDetail, Offer} from '../../rest.service';

@Component({
  selector: 'app-offer',
  templateUrl: './offer.component.html',
  styleUrls: ['./offer.component.css']
})
export class OfferComponent implements OnInit {

  @Input() id: number | undefined;
  offer: OfferDetail | undefined
  constructor(public rest: RestService) {
  }

  ngOnInit(): void {
    this.getOffer()
  }

  getOffer(): void {
    this.rest.getOffer(String(this.id)).subscribe((resp: any) => {
      this.offer = resp;
      console.log(this.offer);
    });
  }
}
