import { Component, OnInit } from '@angular/core';
import { RestService, Offer } from '../rest.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  offers: Offer[] = [];
  constructor(
    public rest: RestService) { }

  ngOnInit(): void {
    this.getOffers()
  }
   getOffers(): void {
    this.rest.getOffers().subscribe((resp: any) => {
      this.offers = resp;
      console.log(this.offers);
    });
  }

}
