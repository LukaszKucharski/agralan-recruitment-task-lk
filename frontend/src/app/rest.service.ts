import { Injectable } from '@angular/core';
import { catchError } from 'rxjs/internal/operators';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { map } from 'rxjs/operators';

export interface Offer {
  id: number;
  title: string;
  price: number;
  category: number;
}

export interface OfferDetail {
  id: number;
  title: string;
  description: string;
  price: number;
  created_at: Date;
  category: number;
}

const endpoint = 'http://localhost:8800/api/';

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor(private http: HttpClient) { }
  getOffers(): Observable<any> {
    return this.http.get<Offer>(endpoint + 'offers').pipe(
      catchError(this.handleError)
    );
  }
  getOffer(id: string): Observable<any> {
    return this.http.get<Offer>(endpoint + 'offers/' + id).pipe(
      catchError(this.handleError)
    );
  }
  private handleError(error: HttpErrorResponse): any {
    if (error.error instanceof ErrorEvent) {
      console.error('An error occurred:', error.error.message);
    } else {
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    return throwError(
      'Something bad happened; please try again later.');
  }
}
