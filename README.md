## Quickstart

### Prerequisities
- `docker`: [installation](https://docs.docker.com/engine/installation/)
- `docker-compose`: [installation](https://docs.docker.com/compose/install/)

### Running

Before starting containers, copy `.env.template` and name it `.env`.

```commandline
cp .env.template .env
```
  
Build and launch containers:

```commandline
docker-compose build
docker-compose up
```

Angular development server page should be available under [http://localhost](http://localhost).

Django Admin page should be available under [http://localhost:8800/admin](http://localhost:8800/admin).

Admin account:
```commandline
login: admin
password: admin
```
